import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    mode: 'hash',
    // mode: 'history',
    base: process.env.BASE_URL,
    routes: [
       { path: '/', redirect: 'longan', }, // 重定位
        // {
        //     path: '/longan',
        //     component: () => import('./components/longan.vue'),
        // },
        // {
        //     path: '/b3d',
        //     component: () => import('./components/b3d/BimModel.vue'),
        // },
    ]
});
